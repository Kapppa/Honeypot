## Overview

Low interaction honeypot application that displays real time attacks. 

Written in Node.js the application listens on some of the most common TCP ports, optionally saving attacks to Redis.

This is Ka's version of Shmakov's excellent work. 

Main chnages:
- Listens on less ports, two configs for Prod and Dev port numbers
- MySQL support removed making place to Redis
- Telnet Login support
- Removed ICMP and Web logging
- Stats are retrieved every minute and pushed to the connected clients
- UI is now in React with Tablet-React for CSS
- Output modules allow to send indicators to OTX, CSIRTG, Abuseipdb...


Currently working on output plugins to send reports to AbuseIPDB, CSIRTG, OTX and DSHIELD (possibly).

This is now meant to be running in a Docker container. 

## Demo

My own instance https://honey.ka.st


## How to Deploy

Two ways two deploy:

### One your local server
We need nodejs and docker, I'll let you find out where to get them

```
git clone https://github.com/KaSt/Honeypot.git
cd Honeypot/ && npm install
[edit config.js or use .env.template to make your own .env and then exec 'export $(grep -v '^#' .env | xargs )']
./build.sh
./run.sh
```

### Deploying as a Docker image
Soon to come

## One more thing 
Make sure the ports you decided to use are not used by the real services

That is it. You should be able to access the app on the port 3010 from your web-browser.
I`d recomment to use a reverse proxy in front of it. I personally use nginx

