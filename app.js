const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const helmet = require("helmet");
const server = require("http").Server(app);
const io = require("socket.io")(server);
const escape = require("escape-html");
const chalk = require("chalk");
var geoip = require('geoip-lite');
const serverConfig = require("./config").serverConfig;
const serviceConfig = require("./config").serviceConfig;
const dbConfig = require("./config").dbConfig;
const CustomSocketServer = require("./lib/server/custom-socket-server");
const helper = require("./lib/server/helper-redis");

const tcp_ports = require("./lib/server/tcp-ports");
const appVersion = "0.1";

const {
  publishCsirtgIndicator,
  publishAbuseIppDbIndicator,
  publishDshieldIndicator,
  publishOtxIndicator,
} = require("./lib/output/output");

let data = [];
let total_requests_number = 0;
let recent_credentials = [];
let recent_attacks = [];
let countries = [];
let services = [];
let credential_stats = [];

/* Socket.io WebSocket Server: on connection */
io.on("connection", (socket) => {
  if (serverConfig.debug) {
    console.log("New client connected "+socket.id);
  }
  socket.emit("init", {
    data: recent_attacks,
    total_requests_number: total_requests_number,
    recent_credentials: recent_credentials
  });
  socket.emit("country_stats", countries);
  socket.emit("service_stats", services);
  socket.emit("credential_stats", credential_stats);


  setInterval(() => {
    socket.emit("country_stats", countries);
    socket.emit("service_stats", services);
    socket.emit("credential_stats", credential_stats);
  }, 60 * 1000); // once a minute

});
io.on('disconnect', () => console.log('Disconnected'));

console.log("Honeypot " + appVersion + " starting...");

/**
 * Custom Socket Server: listening on some common TCP ports
 * @see: ./lib/server/tcp-ports
 */
for (let port in tcp_ports) {
  CustomSocketServer(port, tcp_ports[port]).on("data", (data) => {
    if (serverConfig.debug) {
      console.log("Emitting data");
    }
    emitData(data);
  });
}


if (dbConfig.redis) {
  /* Redis Helper */
  new helper.Redis()
    .on("total_requests_number", (count) => {
      total_requests_number = count;
    })
    .on("recent_credentials", (rows) => {
      // Returns recent SSH/FTP/Telnet usernames/passwords
      recent_credentials = rows;
    })
    .on("recent_attacks", (attacks) => {
      recent_attacks = attacks;
    })
    .on("country_stats", (country_stats) => {
      countries = country_stats;
    })
    .on("service_stats", (service_stats) => {
      services = service_stats;
    })
    .on("credential_stats", (credentialStats) => {
      credential_stats = credentialStats;
    });
  }
  

/* Express App */
app.enable("trust proxy", 1);
app.use(helmet());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static("honeypot-ui/build/"));
app.get("/", (req, res) => {
  res.sendFile("./honeypot-ui/build/index.html", {
    root: __dirname,
    lastModified: false,
    headers: {
      "Cache-Control": "no-cache, no-store, must-revalidate",
      Expires: "0",
    },
  });
});

app.all("*", (req, res) => {
  if (req.hostname === serverConfig.hostname || req.hostname === serverConfig.server_ip) {
    let response = req.hostname
      ? req.method + " " + req.protocol + "://" + req.hostname + req.originalUrl
      : req.method + " " + req.originalUrl;
    if (req.body.length !== 0)
      response += "\r\n\r\n" + helper.formatHeaders(req.body);
    res.status(200).send("<pre>" + escape(response) + "</pre>");
  } else {
    res.sendStatus(404);
  }
});
const server_port = 30101;
server.listen(server_port);
console.log(
  chalk.bgGreen.bold("Info:") +
    ` HTTP Server running at http://${serverConfig.server_ip}:${server_port}/`
);

/**
 * Emits data to the WebSocket clients and also saves it in the MySQL database
 * @param item
 */
const emitData = (item) => {
  var geo = geoip.lookup(item.ip);
  item.timestamp = new Date();
  total_requests_number++;
  item.total_requests_number = total_requests_number;
  item.ip = helper.formatIpAddress(item.ip);
  data[data.length] = item;
  if (geo) {
    if (geo.country) {
      item.country = geo.country;
    }
    if (geo.region) {
      item.region = geo.region;
    }
    if (geo.city) {
      item.city = geo.city;
    }
    if (geo.ll) {
      item.latitude = geo.ll[0];
      item.longitude = geo.ll[1];
    }
  }
  io.emit("broadcast", item);

  if (dbConfig.redis) {
    helper.saveToDatabase(item);
  }
};

/* Cleaning Up Old Data */
setInterval(() => {
  data = helper.removeOldData(data);
}, 1000);

/* We need to manually kill tcpdump process in the case of program termination signal */
const terminate = () => {
  try {
    ping.tcpdumpProcess.kill();
  } catch (error) {}

  server.close(() => {
    process.exit(0);
  });
};

process.on("uncaughtException", function (err) {
  console.log(err);
});

process.on("SIGTERM", terminate);
process.on("SIGINT", terminate);
