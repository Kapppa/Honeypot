FROM debian

RUN apt update
RUN apt install git curl wget gnupg dirmngr bash tmux vim htop procps bash redis -y
RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.37.2/install.sh | bash
RUN . /root/.bashrc && nvm install node && npm i -g yarn

COPY . honey/
RUN . /root/.bashrc && cd honey && npm install
RUN . /root/.bashrc && cd honey/honeypot-ui && npm install && yarn build

COPY config.js honey
COPY Docker/starthoney.sh /usr/bin
RUN chmod +x /usr/bin/starthoney.sh


ENTRYPOINT "/usr/bin/starthoney.sh"
