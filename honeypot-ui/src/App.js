import React, { useState, useEffect } from "react";
import { Card, Table, Grid, Page } from "tabler-react";
import Flag from 'react-world-flags'
import socketIOClient from "socket.io-client";
import FavIcon from 'react-fav-icons';

import ReactTooltip from 'react-tooltip';

import "tabler-react/dist/Tabler.css";


var PieChart = require("react-chartjs").Pie;

let socket = null;

function App() {
  const [attacks, setAttacks] = useState([]);
  const [credentials, setCredentials] = useState([]);
  const [currentStats, setcurrentStats] = useState({});
  const [currentAttacks, setcurrentAttacks] = useState(0);
  const [countries, setCountries] = useState([]);
  const [services, setServices] = useState([]);
  const [credentialStats, setCredentialStats] = useState([]);


  useEffect(() => {
    
    const hostname = typeof window !== 'undefined' && window.location.hostname ?
               (window.location.hostname === "localhost" ? window.location.hostname + ":30101" : window.location.hostname )  : '';
    
    socket = socketIOClient(hostname);
    socket.connect();
    socket.on("connection", data => {
      console.log("Connection");
    });
    socket.on("init", data => {
      console.log("Init data");
      if (data.total_requests_number) {
        setcurrentStats({ total_requests_number: data.total_requests_number, requests_since_launch: data.requests_since_launch });
      }
      if (data.recent_credentials && data.recent_credentials.length > 0) {
        setCredentials(data.recent_credentials);
      }
      if (data.data && data.data.length > 0) {
        setAttacks(data.data);
      }
    });
    socket.on("broadcast", data => {
      console.log("Broadcast");
      if (data.total_requests_number) {
        setcurrentStats({ total_requests_number: data.total_requests_number, requests_since_launch: data.requests_since_launch });
      }
      if (data.ip) {
        setAttacks(prevItems => [data, ...prevItems].slice(0,10));
        setCredentials(prevItems => [data, ...prevItems].slice(0,10));
        setcurrentAttacks(prevCurrentAttacks => prevCurrentAttacks+1);
      }
    });
    socket.on("country_stats", data => {
      setCountries(data);
    })
    socket.on("service_stats", data => {
      setServices(data);
    })
    socket.on("credential_stats", data => {
      setCredentialStats(data);
    })        
  }, []);

  let credentialsRows = [];
  if (credentials.length > 0) {
    credentials.map((data, index) => {
      return credentialsRows.push(
        <Table.Row>
          <Table.Col>{data.username}</Table.Col>
          <Table.Col>{data.password}</Table.Col>
        </Table.Row>);
    })
  };

  let attackRows = [];
  if (attacks.length > 0) {
    attacks.map((data, index) => {
      let timeStamp = new Date(data.timestamp);
      const timeDisplay = timeStamp.getDate()+"/"+(timeStamp.getMonth()+1)+"/"+timeStamp.getFullYear()+ " "+
           ('0' + timeStamp.getHours()).slice(-2) + ':' + ('0' + timeStamp.getMinutes()).slice(-2) + ':' + ('0' + timeStamp.getSeconds()).slice(-2)
      return attackRows.push(
        <Table.Row>
          <Table.Col>
            <Flag code={data.country} height="16" alt={data.country} title={data.country}/>
            {' '}{data.ip}
          </Table.Col>
          <Table.Col>
            {data.request ? 
             <React.Fragment>
             <a
              style={{textDecoration: "none", color: "#495057", cursor: "default"}}
              href="#here"
              data-for="main"
              data-tip={data.request}
              data-iscapture="true"
            >{data.service}</a><ReactTooltip id="main" /></React.Fragment> : ""}
            </Table.Col>
          <Table.Col>{timeDisplay}</Table.Col>
        </Table.Row>);
    });
  }

  return (
      <Page.Content title="Honeypot Dashboard">
        <FavIcon text={currentStats.total_requests_number ? currentStats.total_requests_number : 0}><p></p></FavIcon>
    <Grid>
      <Grid.Row>
        <Grid.Col md={3}>
        <Card className="d-flex align-items-center">
          <Card.Header>Total Attacks</Card.Header>
           <Card.Body align="center"><h2>{currentStats.total_requests_number}</h2></Card.Body> 
          </Card>  
          <Card className="d-flex align-items-center">
          <Card.Header>Attacks since your visit</Card.Header>
           <Card.Body align="center"><h2>{currentAttacks}</h2></Card.Body> 
          </Card>  
        </Grid.Col>
       
        <Grid.Col md={3}>
        <Card  className="d-flex align-items-center">
          <Card.Header>Origin</Card.Header>
          <Card.Body>
        <PieChart data={countries} width="185" height="185"/>
        </Card.Body> 
          </Card> 
        </Grid.Col>
        <Grid.Col md={3}>
        <Card className="d-flex align-items-center">
          <Card.Header>Service</Card.Header>
          <Card.Body>
        <PieChart data={services} width="185" height="185"/>
        </Card.Body> 
          </Card> 
        </Grid.Col>
        <Grid.Col md={3}>
        <Card className="d-flex align-items-center">
          <Card.Header>Top Credentials</Card.Header>
          <Card.Body>
        <PieChart data={credentialStats} width="185" height="185"/>
        </Card.Body> 
          </Card> 
        </Grid.Col>
      </Grid.Row>
      <Grid.Row>
        <Grid.Col md={6}>
          <Card className="d-flex align-items-flex-end">
            <Card.Header>
              <Card.Title>Recent Attacks</Card.Title>
            </Card.Header>
            <Card.Body>
              <Table>
                <Table.Header>
                  <Table.ColHeader>Origin</Table.ColHeader>
                  <Table.ColHeader>Service</Table.ColHeader>
                  <Table.ColHeader>Time</Table.ColHeader>
                </Table.Header>
                <Table.Body>
                  {attacks.length>0 && attackRows}
                </Table.Body>
              </Table>
            </Card.Body>
          </Card>
        </Grid.Col>
        <Grid.Col md={6}>
          <Card>
            <Card.Header>
              <Card.Title>Recent Credentials</Card.Title>
            </Card.Header>
            <Card.Body>
              <Table>
                <Table.Header>
                  <Table.ColHeader>User</Table.ColHeader>
                  <Table.ColHeader>Password</Table.ColHeader>
                </Table.Header>
                <Table.Body>
                  {credentialsRows}
                </Table.Body>
              </Table>
            </Card.Body>
          </Card>
        </Grid.Col>
      </Grid.Row>
    </Grid>
    </Page.Content>
  );
}

export default App;
