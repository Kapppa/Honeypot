const serverConfig =  {
	server_ip: process.env.HONEYPOT_SERVER_IP,
	// Hostname of the server, e.g. honey.ka.st
	hostname: process.env.HONEYPOT_HOSTNAME ? process.env.HONEYPOT_HOSTNAME : "localhost",
	//Debug
	debug: process.env.HONEYPOT_DEBUG,
	env: process.env.HONEYPOT_ENV
};

const dbConfig = {
	redis: process.env.HONEYPOT_REDIS
};

const serviceConfig = {
	telnet: {
		helloMessage: "----------------------------------------------------------------------\n" +
		"\n" +
		"WOPR  EXECUTION ORDER\n" +
		"KA36.948.72\n" +
		"\n" +
		"PART ONE: R O N C T T L\n" +
		"PART TWO: 07:20:35\n" +
		"----------------------------------------------------------------------\n" +
		"\n" +
		"All breach attempts are reported and investigated\n" +
		"Unauthorised access is punishable by law\n" +
		"\n" +
		"----------------------------------------------------------------------\n" +
		"\n",
		authentication: {
			askForUserMessage: "User:",
			askForPasswdMessage: "Password:"
		}
	}
};

const outputConfig = {
		enabled: process.env.HONEYPOT_OUTPUT_ENABLED,
		csirtg: {
			enabled: process.env.HONEYPOT_OUTPUT_CSIRTG_ENABLED,
			userId:  process.env.HONEYPOT_OUTPUT_CSIRTG_USERID,
			feed:    process.env.HONEYPOT_OUTPUT_CSIRTG_FEED,
			apiKey:  process.env.HONEYPOT_OUTPUT_CSIRTG_APIKEY
		},
		abuseipdb: {
			enabled: process.env.HONEYPOT_OUTPUT_ABUSEIPDB_ENABLED,
			categories: process.env.HONEYPOT_OUTPUT_ABUSEIPDB_CATEGORIES,
			apiKey: process.env.HONEYPOT_OUTPUT_ABUSEIPDB_APIKEY
		},
		dshield: {
			enabled: process.env.HONEYPOT_OUTPUT_DSHIELD_ENABLED,
			batch: process.env.HONEYPOT_OUTPUT_DSHIELD_BATCH,
			apiKey: process.env.HONEYPOT_OUTPUT_DSHIELD_APIKEY
		},
		otx: {
			enabled: process.env.HONEYPOT_OUTPUT_OTX_ENABLED,
			pulse: process.env.HONEYPOT_OUTPUT_OTX_PULSE,
			apiKey: process.env.HONEYPOT_OUTPUT_OTX_APIKEY
		}
};

if (serverConfig.hostname.length === 0) serverConfig.hostname = serverConfig.server_ip;

module.exports = {serverConfig, dbConfig, serviceConfig, outputConfig};