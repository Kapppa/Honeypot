"use strict";

const redis = require("redis");
const client = redis.createClient();
const EventEmitter = require("events");
const chalk = require("chalk");


client.on("error", function (error) {
    console.error(error);
});


class Redis extends EventEmitter {
    constructor() {
        super();
        this.init();
    }

    init() {
        this.getTotalRequestsNumber();
        this.getRecentCredentials();
        this.getRecentAttacks();
        this.getCountryStats();
        this.getServiceStats();
        this.getCredentialStats();

        setInterval(() => {
            this.getRecentCredentials();
            this.getRecentAttacks();
            this.getCountryStats();
            this.getServiceStats();
            this.getCredentialStats();
        }, 60 * 1000); // once a minute

        setInterval(() => {
            client.bgsave();
        }, 5 * 60 * 1000); // once every 5 minutes
    }

    getTotalRequestsNumber() {
        const _self = this;
        client.get("attackcount", function (err, reply) {
            if (err) {
                console.log(err);
                return;
            }
            _self.emit("total_requests_number", reply);
        });
    }

    getRecentCredentials() {
        const _self = this;
        client.zrevrange("attacks", 0, 9, function (err, reply) {
            if (err) {
                console.log(err);
                return;
            }
            let rows = [];
            reply.forEach((row) => {
                const attack = JSON.parse(row);
                if (attack.username && attack.password) {
                    rows.push({ username: attack.username, password: attack.password });
                }
            });
            _self.emit("recent_credentials", rows);
        })
    }

    getRecentAttacks() {
        const _self = this;
        client.zrevrange("attacks", 0, 9, function (err, reply) {
            if (err) {
                console.log(err);
                return;
            }
            let rows = [];
            reply.forEach((row) => {
                const attack = JSON.parse(row);
                rows.push({ ip: attack.ip, service: attack.service, request: attack.request, username: attack.username, 
                    password: attack.password, country: attack.country, city: attack.city, region: attack.region, 
                    latitude: attack.latitude, longitude: attack.longitude, timestamp: attack.timestamp });
            });
            _self.emit("recent_attacks", rows);
        })    
    }

    getCountryStats() {
        const _self = this;
        client.hgetall("country", function(err, reply) {
            if (err) {
                console.log(err);
            }
            let data = [];
            let series = [];
            if (reply) {
                for (var index in reply) {
                    data.push({value: reply[index], label: index});                
                }
            }
            _self.emit("country_stats", data);
        })
    }

    getServiceStats() {
        const _self = this;
        client.hgetall("service", function(err, reply) {
            if (err) {
                console.log(err);
            }
            let data = [];
            let series = [];
            if (reply) {
                for (var index in reply) {
                    data.push({value: reply[index], label: index});                
                }
            }
            _self.emit("service_stats", data);
        })
    }
    getCredentialStats() {
        const _self = this;
        client.zrevrange("credentials", 0,9, 'withscores', function(err, reply) {
            if (err) {
                console.log(err);
            }

            let data = [];
            if (reply) {
                let i = 0;
                while ( i <= reply.length-2) {
                    data.push({value: reply[i+1], label: reply[i]});
                    i = i +2;                
                }
            }
            _self.emit("credential_stats", data);
        })
    }
}

const saveToDatabase = (item) => {
    let request = {
        ip: item.ip,
        service: item.service,
        request: item.request,
        request_headers: item.request_headers,
        username: null,
        password: null,
        country: null, 
        city: null, 
        region: null,
        latitude: null,
        longitude: null,
        timestamp: Date.now()    
    };

    if ("username" in item) request.username = item["username"];
    if ("password" in item) request.password = item["password"];
    if ("country" in item) request.country = item["country"];
    if ("city" in item) request.city = item["city"];
    if ("region" in item) request.region = item["region"];
    if ("latitude" in item) request.latitude = item["latitude"];
    if ("longitude" in item) request.longitude = item["longitude"];

    console.log(request.timestamp);
    client.multi()
    .zadd("attacks", new Date().getDate(), JSON.stringify(request))
    .incr("attackcount")
    .exec();

    if (request.country) {
        client.hincrby("country", request.country, 1, function (err, ok) {
            if (err) console.log(err);
        });
    }
    if (request.service) {
        client.hincrby("service", request.service, 1, function (err, ok) {
            if (err) console.log(err);
        });
    }
    if (request.username) {
        const username = request.username;
        const password = request.password ? request.password : "";
        client.zincrby("credentials", 1, username+":"+password, function (err, ok) {
            if (err) console.log(err);
        });
    }
};

const formatHeaders = (headers, indent) => {
    if (typeof headers !== "object" || headers.length === 0) return;
    indent = indent ? indent : "";
    let s = "";
    for (let key in headers) {
        let val = headers[key];
        if (typeof val === "object" && val !== null) {
            s += key + ":\r\n";
            s += formatHeaders(val, indent + " - ");
        } else s += indent + key + ": " + val + "\r\n";
    }

    return s;
};

const formatIpAddress = (address) => {
    if (address.length !== 0 && address.substr(0, 7) === "::ffff:")
        return address.substr(7);

    return address;
};

const removeOldData = (data) => {
    for (let i = 0; i < data.length; i++) {
        if (data.length <= 25) return data;
        let item = data[i];
        if (Date.now() - item.timestamp > 2000) {
            data.splice(i, 1);
        }
    }
    return data;
};

module.exports = {
    formatHeaders: formatHeaders,
    saveToDatabase: saveToDatabase,
    formatIpAddress: formatIpAddress,
    removeOldData: removeOldData,
    Redis: Redis,
};
