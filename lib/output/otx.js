const https = require("https");
const serverConfig = require("../../config").serverConfig;
const outputConfig = require("../../config").outputConfig;

const indicatorTemplate = {
  indicators: {
    add: [
      {
        type: "IPv4",
        indicator: "",
        role: "bruteforce",
        title: "",
      },
    ],
  },
};

const publishOtxIndicator = function (data) {
  if (outputConfig.otx.enabled) {
    var indicator = indicatorTemplate;
    indicatorTemplate.indicators.add[0].indicator = data.ip;
    indicatorTemplate.indicators.add[0].title =
      data.service + " bruteforce attack (Lu)";

    var options = {
      hostname: "otx.alienvault.com",
      port: 443,
      path: "/api/v1/pulses/" + outputConfig.otx.pulse,
      method: "POST",
      headers: {
        "content-type": "application/json",
        Accept: "application/json",
        "X-OTX-API-KEY": outputConfig.otx.apiKey,
        "User-Agent": "OTX .Net SDK",
      },
    };

    if (serverConfig.debug) {
      console.log("Sending indicator to otx");
    }
    var req = https.request(options, (res) => {
      if (serverConfig.debug) {
        console.log("statusCode:", res.statusCode);
        console.log("headers:", res.headers);

        res.on("data", (d) => {
          process.stdout.write(d);
        });
      }
    });
    req.on("error", (e) => {
      console.error(e);
    });

    req.write(JSON.stringify(indicator));
    req.end();
  }
};

module.exports = publishOtxIndicator;
