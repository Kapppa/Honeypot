const https = require("https");
const serverConfig = require("../../config").serverConfig;
const outputConfig = require("../../config").outputConfig;

const indicatorTemplate = {
  indicator: {
    indicator: "",
    itype: "ipv4",
    description: "",
    tags: ["lu", "attack", "bruteforce", "scanning"],
  },
};

const publishCsirtgIndicator = function (data) {
  if (outputConfig.csirtg.enabled) {
    var indicator = indicatorTemplate;
    indicator.indicator.indicator = data.ip;
    indicator.indicator.description = data.service + " connection attempt";
    indicator.indicator.tags.push(data.service);

    var options = {
      hostname: "csirtg.io",
      port: 443,
      path:
        "/api/users/" +
        outputConfig.csirtg.userId +
        "/feeds/" +
        outputConfig.csirtg.feed +
        "/indicators",
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Token token=" + outputConfig.csirtg.apiKey,
      },
    };

    if (serverConfig.debug) {
      console.log("Sending indicator to csirtg");
    }

    var req = https.request(options, (res) => {
      if (serverConfig.debug) {
        console.log("statusCode:", res.statusCode);
        console.log("headers:", res.headers);

        res.on("data", (d) => {
          process.stdout.write(d);
        });
      }
    });
    req.on("error", (e) => {
      console.error(e);
    });

    req.write(JSON.stringify(indicator));
    req.end();
  }
};

module.exports = publishCsirtgIndicator;
