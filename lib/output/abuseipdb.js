const https = require("https");
const serverConfig = require("../../config").serverConfig;
const outputConfig = require("../../config").outputConfig;

const publishAbuseIpDbIndicator = function (data) {
  if (outputConfig.abuseipdb.enabled) {
    var options = {
      hostname: "api.abuseipdb.com",
      port: 443,
      path:
        "/api/v2/report?ip=" +
        data.ip +
        "&categories=" +
        outputConfig.abuseipdb.categories,
      method: "GET",
      headers: {
        "Content-Type": "application/urlencoded",
        Key: outputConfig.abuseipdb.apiKey,
        Accept: "application/json",
      },
    };

    if (serverConfig.debug) {
      console.log("Sending indicator to abuseipdb");
    }
    var req = https.request(options, (res) => {
      if (serverConfig.debug) {
        console.log("statusCode:", res.statusCode);
        console.log("headers:", res.headers);

        res.on("data", (d) => {
          process.stdout.write(d);
        });
      }
    });
    req.on("error", (e) => {
      console.error(e);
    });

    req.end();
  }
};

module.exports = publishAbuseIpDbIndicator;
