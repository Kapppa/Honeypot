const publishCsirtgIndicator = require("./csirtg");
const publishAbuseIppDbIndicator = require("./abuseipdb");
const publishDshieldIndicator = require("./dshield");
const publishOtxIndicator = require("./otx");
const serverConfig = require("../../config").serverConfig;
const outputConfig = require("../../config").outputConfig;

const REPORTING_COOLDOWN = 3600000;

const publishIndicator = function (data, indicators) {
  if (serverConfig.debug) {
    console.log(
      "Received and publishing attack type " + data.service + " from " + data.ip
    );
    console.log("Current atteack log count: " + indicators.length);
  }

  var alreadyThere = false;
  for (var i = 0; i < indicators.length; i++) {
    if (indicators[i].ip == data.ip) {
      var d = new Date();
      if (indicators[i].lastSeen + REPORTING_COOLDOWN > d.getTime()) {
        if (serverConfig.debug) {
          console.log(
            "Same guy that showed up less than cooldown time go, not publishing"
          );
        }
        alreadyThere = true;
      } else {
        if (serverConfig.debug) {
          console.log(
            "Same guy that showed up more than cooldown time go, now publishing again"
          );
        }
        alreadyThere = false;
      }
      indicators[i].lastSeen = d.getTime();
    }
  }
  if (!alreadyThere) {
    if (serverConfig.debug) {
      console.log("Adding new indicator to array");
    }
    var d = new Date();
    const indicator = {
      ip: data.ip,
      service: data.service,
      lastSeen: d.getTime(),
    };
    indicators.push(indicator);

    if (outputConfig.csirtg.enabled) {
      publishCsirtgIndicator(data);
    }
    if (outputConfig.abuseipdb.enabled) {
      publishAbuseIppDbIndicator(data);
    }
    if (outputConfig.dshield.enabled) {
      publishDshieldIndicator(data);
    }
    if (outputConfig.otx.enabled) {
      publishOtxIndicator(data);
    }
  }
};

module.exports = publishIndicator;
